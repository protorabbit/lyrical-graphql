import gql from "graphql-tag";

export const querySongs = gql`
  {
    songs {
      title
      id
    }
  }
`;

export const SONG_DETAIL = gql`
  query GetSong($id: ID!) {
    song(id: $id) {
      id
      title
      lyrics {
        id
        content
        likes
      }
    }
  }
`;

export const ADD_LYRIC = gql`
  mutation AddLyric($songID: ID, $content: String) {
    addLyricToSong(content: $content, songId: $songID) {
      id
      lyrics {
        id
        content
      }
    }
  }
`;

export const LIKE_LYRIC = gql`
  mutation LikeLyric($id: ID) {
    likeLyric(id: $id) {
      id
      likes
    }
  }
`;
