import "./style/style.css";
import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";
import SongList from "./components/SongList";
import { ApolloClient } from "apollo-client";
import { InMemoryCache, NormalizedCacheObject } from "apollo-cache-inmemory";
import { HttpLink } from "apollo-link-http";
import SongListFc from "./components/SongListFc";
import App from "./components/App";
import CreateSong from "./components/CreateSong";

import { HashRouter, Switch, Route, Link, useParams } from "react-router-dom";
import SongDetail from "./components/SongDetail";

const cache = new InMemoryCache();
const link = new HttpLink({
  uri: "http://localhost:4000/graphql"
});

const client = new ApolloClient({
  cache,
  link
});

const Root = () => {
  return (
    <ApolloProvider client={client}>
      <HashRouter>
        <App>
          <Route exact path="/" component={SongListFc} />
          <Route path="/songs/new" component={CreateSong} />
          <Route path="/songs/:id" component={SongDetail} />
        </App>
      </HashRouter>
    </ApolloProvider>
  );
};

ReactDOM.render(<Root />, document.querySelector("#root"));
