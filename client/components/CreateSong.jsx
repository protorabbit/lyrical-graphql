import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { Link, useHistory } from "react-router-dom";
import { querySongs } from "../query";

const ADD_SONG = gql`
  mutation AddSong($title: String) {
    addSong(title: $title) {
      id
    }
  }
`;

const CreateSong = props => {
  const history = useHistory();
  const [title, setTitle] = useState("");
  const [addSong, { data }] = useMutation(ADD_SONG, {
    onCompleted: () => {
      //
      //console.log("dede");
      history.push("/");
    }
  });

  const onSubmit = event => {
    event.preventDefault();
    addSong({ variables: { title }, refetchQueries: [{ query: querySongs }] });
  };

  return (
    <div>
      <Link to="/">Back</Link>
      <h3>Create song</h3>
      <form onSubmit={e => onSubmit(e)}>
        <label>Song title</label>
        <input
          type="text"
          value={title}
          onChange={event => setTitle(event.target.value)}
        ></input>
        <button className="btn" type="submit">
          Add song
        </button>
      </form>
    </div>
  );
};

export default CreateSong;
