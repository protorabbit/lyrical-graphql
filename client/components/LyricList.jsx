import React, { Component } from "react";
import gql from "graphql-tag";

import { useQuery, useMutation } from "@apollo/react-hooks";

import { LIKE_LYRIC } from "../query";

const LyricList = props => {
  const [likeLyric, { data: dataLike }] = useMutation(LIKE_LYRIC, {
    onCompleted: () => {
      //
      //refetchSongsDetail();
      console.log("liked");
    }
  });

  const onLike = (id, likes) => {
    likeLyric({
      variables: { id },
      optimisticResponse: {
        __typename: "Mutation",
        likeLyric: {
          id: id,
          __typename: "LyricType",
          likes: likes + 1
        }
      }
    });
  };

  if (!props.lyrics) return null;
  return (
    <ul className="collection">
      {props.lyrics.map(lyric => (
        <li className="collection-item" key={lyric.id}>
          {lyric.content}
          <div className="vote-box">
            <span>{lyric.likes}</span>
            <i
              className="material-icons"
              onClick={() => onLike(lyric.id, lyric.likes)}
            >
              thumb_up
            </i>
          </div>
        </li>
      ))}
    </ul>
  );
};
export default LyricList;
