import React, { Component } from "react";
import gql from "graphql-tag";

import { useQuery, useMutation } from "@apollo/react-hooks";
import { Link } from "react-router-dom";
import { querySongs } from "../query";

const DEL_SONG = gql`
  mutation Del($id: ID) {
    deleteSong(id: $id) {
      id
    }
  }
`;

const SongListFc = () => {
  const { loading, error, data, refetch: refetchSongs } = useQuery(querySongs);
  const [delSong] = useMutation(DEL_SONG, {
    //refetchQueries: [{ query: querySongs }], kan ook
    onCompleted: () => {
      //
      refetchSongs();
      //console.log("dede");
    }
  });

  const onSongDelete = id => {
    delSong({
      variables: {
        id
      }
    });
  };
  if (!data) return null;
  if (loading) return <strong>Loading</strong>;
  return (
    <div>
      <strong>Songs</strong>
      <ul className="collection">
        {data.songs.map(({ id, title }) => (
          <li className="collection-item" key={id}>
            <Link to={`/songs/${id}`}>{title}</Link>
            <i className="material-icons " onClick={() => onSongDelete(id)}>
              delete
            </i>
          </li>
        ))}
      </ul>
      <Link to="/songs/new" className="btn-floating btn-large red right">
        <i className="material-icons">add</i>
      </Link>
    </div>
  );
};

export default SongListFc;
