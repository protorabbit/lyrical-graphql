import React, { Component } from "react";
import gql from "graphql-tag";

import { useQuery, useMutation } from "@apollo/react-hooks";
import { Link } from "react-router-dom";
import { SONG_DETAIL, LIKE_LYRIC } from "../query";
import { useParams } from "react-router-dom";
import CreateLyric from "./CreateLyric";
import LyricList from "./LyricList";
const SongDetail = props => {
  const { id } = useParams();
  const {
    loading,
    error,
    data,
    refetch: refetchSongsDetail
  } = useQuery(SONG_DETAIL, { variables: { id } });

  if (!data) return <div>No data</div>;

  if (loading) {
    return <div>loading...</div>;
  }
  return (
    <div>
      <Link to="/">Back</Link>
      <h3>Song:{data.song.title}</h3>
      <LyricList lyrics={data.song.lyrics} />
      <CreateLyric songId={id} />
    </div>
  );
};

export default SongDetail;
