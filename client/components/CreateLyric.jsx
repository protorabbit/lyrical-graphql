import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { Link, useHistory } from "react-router-dom";
import { ADD_LYRIC, SONG_DETAIL } from "../query";

const CreateLyric = props => {
  const history = useHistory();
  const [lyric, setLyric] = useState("");
  const [addLyric, { data }] = useMutation(ADD_LYRIC, {
    onCompleted: () => {
      //history.push("/");
      setLyric("");
    }
  });

  const onSubmit = event => {
    event.preventDefault();
    addLyric({
      variables: { content: lyric, songID: props.songId },
      refetchQueries: [{ query: SONG_DETAIL, variables: { id: props.songId } }]
    });
  };

  return (
    <form onSubmit={e => onSubmit(e)}>
      <label>Create lyric</label>
      <input
        type="text"
        value={lyric}
        onChange={event => setLyric(event.target.value)}
      ></input>
      <button type="submit" className="btn">
        Add lyric
      </button>
    </form>
  );
};

export default CreateLyric;
